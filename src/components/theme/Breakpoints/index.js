export {
  default,
  BreakpointsPropTypes,
  BreakpointsDefaultProps,
  breakpoint,
} from "./Breakpoints";
