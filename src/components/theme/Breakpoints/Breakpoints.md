## Breakpoints

- This design system follows the mobile first design paradigm. See http://mobile-first.abookapart.com/
- This means progressive enhancement. We start from designing for the smallest device (let's say 240x320px, a Nokia 8110 4G phone without touchscreen) with the most basic feature set (typography and images only, no complex interactions) then enhance the design as bigger screens come in with more features.
- Technically speaking all media queries will be based on `min-width` instead of `max-width`.
