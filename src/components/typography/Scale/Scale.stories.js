import React from "react";
import Scale from "./Scale";

export default {
  component: Scale,
  title: "Scale"
};

const Template = args => <Scale {...args} />;

export const Default = Template.bind({});
Default.args = {};
