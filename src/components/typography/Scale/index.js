export {
  default,
  ScalePropTypes,
  ScaleDefaultProps,
  scale,
  scaleValue,
} from "./Scale";
