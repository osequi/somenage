export {
  default,
  SpacingPropTypes,
  SpacingDefaultProps,
  spacingMarginTop,
} from "./Spacing";
