import React from "react";
import Spacing from "./Spacing";

export default {
  component: Spacing,
  title: "Spacing"
};

const Template = args => <Spacing {...args} />;

export const Default = Template.bind({});
Default.args = {};
