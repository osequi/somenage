export {
  default,
  FontsPropTypes,
  FontsDefaultProps,
  getFont,
  getFontCss,
  font,
} from "./Fonts";
