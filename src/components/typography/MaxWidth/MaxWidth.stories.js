import React from "react";
import MaxWidth from "./MaxWidth";

export default {
  component: MaxWidth,
  title: "MaxWidth"
};

const Template = args => <MaxWidth {...args} />;

export const Default = Template.bind({});
Default.args = {};
